package com.otus.study.factory

import com.fasterxml.jackson.databind.ObjectMapper
import com.otus.study.factory.dto.RequestPayload
import com.otus.study.factory.file.read.FileReaderFacade
import com.otus.study.factory.file.write.CsvFileWriter
import com.otus.study.factory.file.write.FileWriterFacade
import com.otus.study.factory.sorter.SorterFactory
import org.springframework.web.bind.annotation.*
import java.nio.file.Paths

@RestController
@RequestMapping(path = ["/sort"])
class Endpoint(private val readerFacade: FileReaderFacade,
               private val sorterFactory: SorterFactory,
               private val writerFactory: FileWriterFacade,
               private val mapper: ObjectMapper) {

    @PostMapping(path = ["/primitive"])
    fun primitiveSort(@RequestBody body: String){

        val payload = mapper.readValue(body, RequestPayload::class.java)

        val filePath = Paths.get(payload.requestFileName)
        var sortFile = readerFacade.readFile(filePath.toFile())
        val data = sortFile.data.toMutableList()

        val sorter = sorterFactory.getSorterByName(payload.sortType, "primitive")
        val sortData = sorter.sort(data)
        println(sortData)

        writerFactory.writeFile(Paths.get(payload.responseFileName).toFile(), sortData)
    }

}