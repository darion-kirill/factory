package com.otus.study.factory.file.write

import java.io.File

interface FileWriterAbstract {
    fun write(file: File, writeData: List<Any>)
    fun getReaderExtension(): String
}