package com.otus.study.factory.file.read

import org.springframework.stereotype.Service
import java.io.IOException
import java.util.*

@Service
class FileReaderFactory(private val availableReaders: List<FileReader>) {

    fun getReaderByExtension(extension: String): FileReader{

        val currentBeanList = getAvailableReaders().filter{ it
                    .getReaderExtension()
                    .lowercase(Locale.getDefault())
                    .equals(extension.lowercase(Locale.getDefault()))
        }

        if(currentBeanList.isEmpty()){
            throw IOException("No reader found for file with extension $extension")
        }

        return currentBeanList[0]
    }

    fun getAvailableReaders() = availableReaders

}