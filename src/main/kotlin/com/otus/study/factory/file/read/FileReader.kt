package com.otus.study.factory.file.read

import com.otus.study.factory.dto.SortFile
import java.io.File

interface FileReader {
    fun read(file: File): SortFile
    fun getReaderExtension(): String
}