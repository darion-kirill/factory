package com.otus.study.factory.file.write

import org.springframework.stereotype.Service
import java.io.IOException
import java.util.*

@Service
class FileWriterFactory(private val availableWriters: List<FileWriterAbstract>) {

    fun getWriterByExtension(extension: String): FileWriterAbstract{

        val currentBeanList = getAvailableWriters().filter{ it
                    .getReaderExtension()
                    .lowercase(Locale.getDefault())
                    .equals(extension.lowercase(Locale.getDefault()))
        }

        if(currentBeanList.isEmpty()){
            throw IOException("No writer found for file with extension $extension")
        }

        return currentBeanList[0]
    }

    fun getAvailableWriters() = availableWriters

}