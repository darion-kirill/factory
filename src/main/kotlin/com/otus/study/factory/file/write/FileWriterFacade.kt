package com.otus.study.factory.file.write

import org.springframework.stereotype.Component
import java.io.File

@Component
class FileWriterFacade(private val writerFactory: FileWriterFactory) {

    fun writeFile(file: File, writeData: List<Any>){
        val currentWriter = writerFactory.getWriterByExtension(file.extension)
        currentWriter.write(file, writeData)
    }

}