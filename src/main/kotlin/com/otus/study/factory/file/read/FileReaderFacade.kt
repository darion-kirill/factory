package com.otus.study.factory.file.read

import com.otus.study.factory.dto.SortFile
import org.springframework.stereotype.Component
import java.io.File

@Component
class FileReaderFacade(private val readerFactory: FileReaderFactory) {

    fun readFile(file: File): SortFile{
        val currentReader = readerFactory.getReaderByExtension(file.extension)
        return currentReader.read(file)
    }

}