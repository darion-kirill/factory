package com.otus.study.factory.file.read

import com.otus.study.factory.config.CsvConfig
import com.otus.study.factory.dto.SortFile
import org.springframework.stereotype.Component
import java.io.File

@Component
class CsvFileReader(private val config: CsvConfig): FileReader {

    private val extension = "csv"

    override fun getReaderExtension() = extension

    override fun read(file: File): SortFile {
        val dataList = mutableListOf<String>()
        file.forEachLine {
            val rowList = mutableListOf<String>()
            it.split(config.getCsvDelimiter()).forEach{ cell -> rowList.add(cell)}
            dataList.addAll(dataList.size, rowList)
        }

        return SortFile(file.name, dataList)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CsvFileReader

        if (config != other.config) return false
        if (extension != other.extension) return false

        return true
    }

}