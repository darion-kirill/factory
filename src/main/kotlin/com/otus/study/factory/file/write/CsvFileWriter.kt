package com.otus.study.factory.file.write

import org.springframework.stereotype.Component
import java.io.File
import java.io.FileWriter
import java.nio.file.Paths
import java.util.stream.Collectors

@Component
class CsvFileWriter: FileWriterAbstract {

    private val extension = "csv"

    override fun getReaderExtension() = extension

   override fun write(file: File, writeData: List<Any>){

        val fileWriter = FileWriter(file)

        fileWriter.write(
            writeData.stream()
                .map { it.toString() }
                .collect(Collectors.joining(";"))
        )

        fileWriter.close()
    }

}