package com.otus.study.factory.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "csv")
class CsvConfig{
    lateinit var delimiter: String
    fun getCsvDelimiter() = delimiter
}