package com.otus.study.factory.dto


import com.fasterxml.jackson.annotation.JsonProperty

data class RequestPayload(
    @JsonProperty("requestFileName")
    val requestFileName: String,
    @JsonProperty("responseFileName")
    val responseFileName: String,
    @JsonProperty("sortType")
    val sortType: String
)