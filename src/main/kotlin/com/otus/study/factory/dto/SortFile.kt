package com.otus.study.factory.dto

data class SortFile(val fileName: String, val data: List<Any>)