package com.otus.study.factory.sorter.impl

import com.otus.study.factory.sorter.Sorter

class InsertionSorter<T: Comparable<T>>: Sorter<T> {

    override fun sort(sortList: MutableList<T>) {
        for (i in 1 until sortList.size) {
            val current = sortList[i]
            var j = i - 1
            while (j >= 0 && current < sortList[j]) {
                sortList[j + 1] = sortList[j]
                j--
            }
            sortList[j + 1] = current
        }

    }

}