package com.otus.study.factory.sorter.impl

import com.otus.study.factory.sorter.Sorter

class MergeSorter<T: Comparable<T>>: Sorter<T> {

    override fun sort(sortList: MutableList<T>){
        mergeSort(sortList, 0, sortList.size - 1)
    }

    private fun merge(sortList: MutableList<T>, low: Int, mid: Int, high: Int){
        val temp = getTempList(sortList, high - low)
        var i = low
        var j = mid + 1
        var k = 0
        while(i <= mid && j <= high){
            temp[k++] = if(sortList[i] < sortList[j]) sortList[i++] else sortList[j++]
        }
        while(i<= mid) temp[k++] = sortList[i++]
        while(j<= high) temp[k++] = sortList[j++]
        for(k2 in temp.indices) sortList[k2 + low] = temp[k2]
    }

    private fun mergeSort(sortList:MutableList<T>, low:Int, high:Int){
        val mid:Int = (low+ high) / 2
        if( low < high){
            mergeSort(sortList, low, mid)
            mergeSort(sortList, mid + 1, high)
            merge(sortList, low, mid, high)
        }
    }

    private fun getTempList(parentList: MutableList<T>, size: Int): MutableList<T>{
        val tempList = mutableListOf<T>()
        var stopFill = false
        var i = 0
        while (!stopFill){
            tempList.add(parentList[i])
            if(tempList.size > size){
                stopFill = true
            }
            i++
        }

        return tempList
    }



}