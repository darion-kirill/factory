package com.otus.study.factory.sorter.impl

import com.otus.study.factory.sorter.Sorter

class SelectionSorter<T: Comparable<T>>: Sorter<T> {

    override fun sort(sortList: MutableList<T>) {
        selectionSort(sortList, 0)
    }

    private fun selectionSort(sortList: MutableList<T>, left: Int) {
        if (left < sortList.size - 1) {
            swap(sortList, left, findMin(sortList, left))
            selectionSort(sortList, left + 1)
        }
    }
    
    private fun findMin(sortList: MutableList<T>, index: Int): Int {
        var min = index - 1
        if (index < sortList.size - 1) min = findMin(sortList, index + 1)
        if (sortList[index] < sortList[min]) min = index
        return min
    }


    private fun swap(sortList: MutableList<T>, index1: Int, index2: Int) {
        val temp = sortList[index1]
        sortList[index1] = sortList[index2]
        sortList[index2] = temp
    }

}