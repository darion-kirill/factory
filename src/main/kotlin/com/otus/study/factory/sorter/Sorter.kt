package com.otus.study.factory.sorter

interface Sorter<T> {
    fun sort(sortList: MutableList<T>)
}