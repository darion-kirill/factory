package com.otus.study.factory.sorter

import com.otus.study.factory.sorter.facade.SorterFacade
import org.springframework.stereotype.Component
import java.io.IOException
import java.util.*

@Component
class SorterFactory(private val availableFacades: List<SorterFacade>) {

    fun getSorterByName(sorterName: String, sorterType: String): SorterFacade {

        val currentBeanList = getAvailableFacades().filter{
            it.getSorterName().lowercase(Locale.getDefault()).equals(sorterName.lowercase(Locale.getDefault()))
            && it.getFacadeType().lowercase(Locale.getDefault()).equals(sorterType.lowercase(Locale.getDefault()))
        }

        if(currentBeanList.isEmpty()){
            throw IOException("The specified sort type was not found $sorterName")
        }

        return currentBeanList[0]
    }

    fun getAvailableFacades() = availableFacades

}