package com.otus.study.factory.sorter.facade.primitive

import com.otus.study.factory.sorter.impl.MergeSorter
import org.springframework.stereotype.Component
import kotlin.streams.toList

@Component
class MergeSorterFacade: PrimitiveSorterFactory() {

    private val sorterName = "merge"

    override fun getSorterName(): String = sorterName

    override fun sort(sortList: MutableList<Any>): List<Any>{

        val intRegex = Regex("[-+]?\\d+")
        val doubleRegex = Regex("((-|\\+)?[0-9]+(\\.[0-9]+)?)+")

        if(sortList.stream().allMatch{it.toString().matches(intRegex)}){
            val longList: MutableList<Long> = sortList.stream()
                .map { it.toString().toLong() }
                .toList()
                .toMutableList()
            MergeSorter<Long>().sort(longList)

            return longList
        }

        if(sortList.stream().allMatch{it.toString().matches(doubleRegex)}) {
            val doubleList: MutableList<Double> = sortList.stream()
                .map { it.toString().toDouble() }
                .toList()
                .toMutableList()
            MergeSorter<Double>().sort(doubleList)

            return doubleList
        }

        val stringList: MutableList<String> = sortList.stream().map { it.toString() }.toList().toMutableList()
        MergeSorter<String>().sort(stringList)

        return stringList
    }

}