package com.otus.study.factory.sorter.facade.primitive

import com.otus.study.factory.sorter.facade.SorterFacade

abstract class PrimitiveSorterFactory: SorterFacade {
    private val factoryType = "primitive"
    override fun getFacadeType(): String = factoryType
}