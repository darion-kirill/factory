package com.otus.study.factory.sorter.facade

interface SorterFacade {
    fun sort(sortList: MutableList<Any>): List<Any>
    fun getSorterName(): String
    fun getFacadeType(): String
}