package com.otus.study.factory.sorter.impl

import com.otus.study.factory.sorter.impl.MergeSorter
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class MergeSorterTest {

    @Test
    fun `testing the correctness of sorting`(){
        val sorter = MergeSorter<Int>()
        val sortArray = mutableListOf(1,5,4,3,6,9,2,0,8,7)
        sorter.sort(sortArray)
        Assertions.assertEquals(sortArray, mutableListOf(0,1,2,3,4,5,6,7,8,9))
    }

}