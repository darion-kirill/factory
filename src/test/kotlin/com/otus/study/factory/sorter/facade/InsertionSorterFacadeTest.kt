package com.otus.study.factory.sorter.facade

import com.otus.study.factory.sorter.facade.primitive.InsertionSorterFacade
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [InsertionSorterFacade::class])
class InsertionSorterFacadeTest {

    @Autowired
    private lateinit var facade: InsertionSorterFacade

    @Test
    fun `testing the correctness of sorting an long list`(){
        val sortArray: MutableList<Any> = mutableListOf("1","5","4","3","6","9","2","0","8","7")
        val expectedResults: List<Any> = listOf(0L,1L,2L,3L,4L,5L,6L,7L,8L,9L)
        val actualResult: List<Any> = facade.sort(sortArray)
        Assertions.assertEquals(expectedResults, actualResult)
    }

    @Test
    fun `testing the correctness of sorting an double list`(){
        val sortArray: MutableList<Any> = mutableListOf("1","5","4.0","3","6","9.2","2","0","8","7")
        val expectedResults: List<Any> = listOf(0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.2)
        val actualResult: List<Any> = facade.sort(sortArray)
        Assertions.assertEquals(expectedResults, actualResult)
    }

    @Test
    fun `testing the correctness of sorting an string list`(){
        val sortArray: MutableList<Any> = mutableListOf("b","c","a","h", "x", "d")
        val expectedResults: List<Any> = listOf("a", "b", "c", "d", "h", "x")
        val actualResult: List<Any> = facade.sort(sortArray)
        Assertions.assertEquals(expectedResults, actualResult)
    }

    @Test
    fun `various data should be sorted as strings`(){
        val sortArray: MutableList<Any> = mutableListOf("b","c","1","h", "6.8", "9")
        val expectedResults: List<Any> = listOf("1", "6.8", "9", "b", "c", "h")
        val actualResult: List<Any> = facade.sort(sortArray)
        Assertions.assertEquals(expectedResults, actualResult)
    }

}