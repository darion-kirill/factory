package com.otus.study.factory

import com.otus.study.factory.sorter.SorterFactory
import com.otus.study.factory.sorter.facade.SorterFacade
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.SpyBean
import java.io.IOException

@SpringBootTest(classes = [SorterFactory::class])
class SorterFactoryTest {

    @SpyBean
    private lateinit var sorterFactory: SorterFactory

    @Test
    fun `the correct file extension should return the correct sorter`(){
        val expectedSorter = Mockito.mock(SorterFacade::class.java)
        Mockito.`when`(expectedSorter.getSorterName()).thenReturn("testSorter")
        Mockito.`when`(expectedSorter.getFacadeType()).thenReturn("primitive")
        Mockito.`when`(sorterFactory.getAvailableFacades()).thenReturn(listOf(expectedSorter))
        val actualSorter = sorterFactory.getSorterByName("testSorter", "primitive")

        Assertions.assertEquals(expectedSorter, actualSorter)
    }

    @Test
    fun `incorrect file extension should throw exception`(){
        val expectedSorter = Mockito.mock(SorterFacade::class.java)
        Mockito.`when`(expectedSorter.getSorterName()).thenReturn("testSorter")
        Mockito.`when`(expectedSorter.getFacadeType()).thenReturn("primitive")
        Mockito.`when`(sorterFactory.getAvailableFacades()).thenReturn(listOf(expectedSorter))

        Assertions.assertThrows(IOException::class.java){sorterFactory.getSorterByName("txt", "primitive")}
    }

}