package com.otus.study.factory

import com.fasterxml.jackson.databind.ObjectMapper
import com.otus.study.factory.config.CsvConfig
import com.otus.study.factory.file.read.CsvFileReader
import com.otus.study.factory.file.read.FileReaderFacade
import com.otus.study.factory.file.read.FileReaderFactory
import com.otus.study.factory.file.write.CsvFileWriter
import com.otus.study.factory.file.write.FileWriterFacade
import com.otus.study.factory.file.write.FileWriterFactory
import com.otus.study.factory.sorter.SorterFactory
import com.otus.study.factory.sorter.facade.primitive.InsertionSorterFacade
import com.otus.study.factory.sorter.facade.primitive.MergeSorterFacade
import com.otus.study.factory.sorter.facade.primitive.SelectionSorterFacade
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.boot.test.mock.mockito.SpyBean
import java.io.File
import java.io.FileReader
import java.nio.file.Files

@SpringBootTest(classes = [FileReaderFacade::class,
    SorterFactory::class,
    FileWriterFacade::class,
    ObjectMapper::class,
    FileReaderFactory::class,
    FileWriterFactory::class,
    SelectionSorterFacade::class,
    InsertionSorterFacade::class,
    MergeSorterFacade::class,
    CsvConfig::class,
    Endpoint::class,
    CsvFileReader::class,
    CsvFileWriter::class
])
class EndpointTest {

    @MockBean
    private lateinit var csvConfig: CsvConfig

    @SpyBean
    private lateinit var fileReaderFactory: FileReaderFactory

    @SpyBean
    private lateinit var sorterFactory: SorterFactory

    @SpyBean
    private lateinit var fileWriterFactory: FileWriterFactory

    @Autowired
    private lateinit var endpoint: Endpoint

    @Autowired
    private lateinit var csvFileReader: CsvFileReader

    @Autowired
    private lateinit var csvFileWriter: CsvFileWriter

    @Test
    fun `insertion sorter testing`(){

        val testRequestString = """
            { 
                "requestFileName": "src/test/resources/test.csv", 
                "responseFileName": "src/test/resources/result.csv", 
                "sortType": "insertion" 
            }
        """.trimIndent()

        Mockito.`when`(fileReaderFactory.getAvailableReaders()).thenReturn(listOf(csvFileReader))
        Mockito.`when`(fileWriterFactory.getAvailableWriters()).thenReturn(listOf(csvFileWriter))
        Mockito.`when`(csvConfig.getCsvDelimiter()).thenReturn(";")

        val testFile = File("src/test/resources/result.csv")
        endpoint.primitiveSort(testRequestString)

        val fileReader = FileReader(testFile)
        val actualRow = fileReader.readLines()
        val expectedRow = listOf("1;1;2;2;3;3;4;4;10;15;43;45")
        Files.delete(testFile.toPath())

        Assertions.assertEquals(expectedRow, actualRow)
    }

    @Test
    fun `selection sorter testing`(){

        val testRequestString = """
            { 
                "requestFileName": "src/test/resources/test.csv", 
                "responseFileName": "src/test/resources/result.csv", 
                "sortType": "selection" 
            }
        """.trimIndent()

        Mockito.`when`(fileReaderFactory.getAvailableReaders()).thenReturn(listOf(csvFileReader))
        Mockito.`when`(fileWriterFactory.getAvailableWriters()).thenReturn(listOf(csvFileWriter))
        Mockito.`when`(csvConfig.getCsvDelimiter()).thenReturn(";")

        val testFile = File("src/test/resources/result.csv")
        endpoint.primitiveSort(testRequestString)

        val fileReader = FileReader(testFile)
        val actualRow = fileReader.readLines()
        val expectedRow = listOf("1;1;2;2;3;3;4;4;10;15;43;45")
        Files.delete(testFile.toPath())

        Assertions.assertEquals(expectedRow, actualRow)
    }

    @Test
    fun `merge sorter testing`(){

        val testRequestString = """
            { 
                "requestFileName": "src/test/resources/test.csv", 
                "responseFileName": "src/test/resources/result.csv", 
                "sortType": "merge" 
            }
        """.trimIndent()

        Mockito.`when`(fileReaderFactory.getAvailableReaders()).thenReturn(listOf(csvFileReader))
        Mockito.`when`(fileWriterFactory.getAvailableWriters()).thenReturn(listOf(csvFileWriter))
        Mockito.`when`(csvConfig.getCsvDelimiter()).thenReturn(";")

        val testFile = File("src/test/resources/result.csv")
        endpoint.primitiveSort(testRequestString)

        val fileReader = FileReader(testFile)
        val actualRow = fileReader.readLines()
        val expectedRow = listOf("1;1;2;2;3;3;4;4;10;15;43;45")
        Files.delete(testFile.toPath())

        Assertions.assertEquals(expectedRow, actualRow)
    }

}