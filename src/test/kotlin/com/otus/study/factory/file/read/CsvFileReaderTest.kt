package com.otus.study.factory.file.read

import com.otus.study.factory.config.CsvConfig
import com.otus.study.factory.dto.SortFile
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import java.nio.file.Paths

@SpringBootTest(classes = [CsvFileReader::class, CsvConfig::class])
class CsvFileReaderTest {

    @Autowired
    private lateinit var csvFileReader: CsvFileReader

    @MockBean
    private lateinit var config: CsvConfig

    @Test
    fun `processing the correct file should return the correct object SortFile`(){
        Mockito.`when`(config.getCsvDelimiter()).thenReturn(";")
        val actualFile = csvFileReader.read(Paths.get("src/test/resources/test.csv").toFile())
        println(actualFile)

        val expectedFile = SortFile("test.csv", listOf("3","4","1","4","2","10","15","2","1","43","45","3"))
        Assertions.assertEquals(expectedFile, actualFile)
    }

}