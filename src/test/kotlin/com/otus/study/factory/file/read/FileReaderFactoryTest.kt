package com.otus.study.factory.file.read

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.SpyBean
import java.io.IOException

@SpringBootTest(classes = [FileReaderFactory::class])
class FileReaderFactoryTest {

    @SpyBean
    private lateinit var readerFactory: FileReaderFactory

    @Test
    fun `the correct file extension should return the correct reader`(){
        val expectedReader = Mockito.mock(FileReader::class.java)
        Mockito.`when`(expectedReader.getReaderExtension()).thenReturn("csv")
        Mockito.`when`(readerFactory.getAvailableReaders()).thenReturn(listOf(expectedReader))
        val actualReader = readerFactory.getReaderByExtension("csv")

        Assertions.assertEquals(expectedReader, actualReader)
    }

    @Test
    fun `incorrect file extension should throw exception`(){
        val expectedReader = Mockito.mock(FileReader::class.java)
        Mockito.`when`(expectedReader.getReaderExtension()).thenReturn("csv")
        Mockito.`when`(readerFactory.getAvailableReaders()).thenReturn(listOf(expectedReader))

        Assertions.assertThrows(IOException::class.java){readerFactory.getReaderByExtension("txt")}
    }

}