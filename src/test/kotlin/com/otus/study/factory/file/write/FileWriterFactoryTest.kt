package com.otus.study.factory.file.write

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.SpyBean
import java.io.IOException

@SpringBootTest(classes = [FileWriterFactory::class])
class FileWriterFactoryTest {

    @SpyBean
    private lateinit var writerFactory: FileWriterFactory

    @Test
    fun `the correct file extension should return the correct writer`(){
        val expectedWriter = Mockito.mock(FileWriterAbstract::class.java)
        Mockito.`when`(expectedWriter.getReaderExtension()).thenReturn("csv")
        Mockito.`when`(writerFactory.getAvailableWriters()).thenReturn(listOf(expectedWriter))
        val actualWriter = writerFactory.getWriterByExtension("csv")

        Assertions.assertEquals(expectedWriter, actualWriter)
    }

    @Test
    fun `incorrect file extension should throw exception`(){
        val expectedWriter = Mockito.mock(FileWriterAbstract::class.java)
        Mockito.`when`(expectedWriter.getReaderExtension()).thenReturn("csv")
        Mockito.`when`(writerFactory.getAvailableWriters()).thenReturn(listOf(expectedWriter))

        Assertions.assertThrows(IOException::class.java){writerFactory.getWriterByExtension("txt")}
    }

}