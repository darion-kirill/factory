package com.otus.study.factory.file.write

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.io.File
import java.io.FileReader
import java.nio.file.Files
import java.util.stream.Collectors

@SpringBootTest(classes = [CsvFileWriter::class])
class CsvFileWriterTest {

    @Autowired
    private lateinit var csvFileWriter: CsvFileWriter

    @Test
    fun `testing the correctness of data entry into the file`(){

        val testFile = File("src/test/resources/result.csv")

        val testData = listOf("1", "3", "32", "4", "31")
        csvFileWriter.write(testFile, testData)

        val fileReader = FileReader(testFile)
        val actualRow = fileReader.readLines()
        val expectedRow = listOf(testData.stream().collect(Collectors.joining(";")))
        Files.delete(testFile.toPath())

        Assertions.assertEquals(expectedRow, actualRow)
    }

}